# Trees

In this class, you will learn about a very common algorithmic tool: trees.
The data structure itself will be motivated by a simple search example and then
implemented and studied from scratch.

Finally, a few examples of trees will be showcased:
- _perfect_ maze generation
- loss-less text compression
- neighborhood optimization in $n$-body systems

# Table of content
- [_Upload your work to the LMS_](#upload-your-work-to-the-lms-toc)
- [_Binary search_](#binary-search-toc)
  - [_Question 1._](#question-1-toc)
  - [_Question 2._](#question-2-toc)
- [_Binary search trees_](#binary-search-trees-toc)
  - [_The tree data structure_](#the-tree-data-structure-toc)
      - [_Question 3._](#question-3-toc)
      - [_Question 4._](#question-4-toc)
      - [_Question 5._](#question-5-toc)
      - [_Question 6._](#question-6-toc)
      - [_Tree traversal_](#tree-traversal-toc)
          - [_Question 7._](#question-7-toc)
  - [_Implementing the BST_](bst.md#implementing-the-bst-toc)
      - [_Question 8._](bst.md#question-8-toc)
    - [_Insertion_](bst.md#insertion-toc)
      - [_Question 9._](bst.md#question-9-toc)
    - [_Search_](bst.md#search-toc)
      - [_Question 10._](bst.md#question-10-toc)
    - [_Deletion_](bst.md#deletion-toc)
      - [_Question 11._](bst.md#question-11-toc)
- [_Huffman encoding_](huffman.md#huffman-encoding-toc)
    - [_Introduction_](huffman.md#introduction-toc)
        - [_Question 12._](huffman.md#question-12-toc)
        - [_Question 13._](huffman.md#question-13-toc)
    - [_Huffman trees_](huffman.md#huffman-trees-toc)
        - [_Question 14._](huffman.md#question-14-toc)
    - [_Coding books_](huffman.md#coding-books-toc)
        - [_Question 15._](huffman.md#question-15-toc)
    - [_Text compression_](huffman.md#text-compression-toc)
        - [_Question 16._](huffman.md#question-16-toc)
    - [_Text decompression_](huffman.md#text-decompression-toc)
        - [_Question 17._](huffman.md#question-17-toc)
        - [_Question 18._](huffman.md#question-18-toc)
    - [_Writing a CLI compression tool_](huffman.md#writing-a-cli-compression-tool-toc)
        - [_The compression format_](huffman.md#the-compression-format-toc)
        - [_Writing to and reading from the disk_](huffman.md#writing-to-and-reading-from-the-disk-toc)
            - [_Question 19._](huffman.md#question-19-toc)
            - [_Question 20._](huffman.md#question-20-toc)
        - [_Wrapping up in a CLI application_](huffman.md#wrapping-up-in-a-cli-application-toc)
            - [_Question 21._](huffman.md#question-21-toc)
- [_Unbalanced trees_ (_optional_)](avl.md#unbalanced-trees-toc-optional)
  - [_Question 22._](avl.md#question-22-toc)
- [_Height-balanced BSTs_ (_optional_)](avl.md#height-balanced-bsts-toc-optional)
  - [_Question 23._](avl.md#question-23-toc)
  - [_Question 24._](avl.md#question-24-toc)
  - [_Question 25._](avl.md#question-25-toc)
  - [_Tree rotations_](avl.md#tree-rotations-toc)
      - [_Question 26._](avl.md#question-26-toc)
      - [_Question 27._](avl.md#question-27-toc)
  - [_Balancing our BSTs_](avl.md#balancing-our-bsts-toc)
      - [_Question 28._](avl.md#question-28-toc)
- [_Maze generation and solving_ (_optional_)](maze.md#maze-generation-and-solving-toc-optional)
  - [_Generating perfect mazes_](maze.md#generating-perfect-mazes-toc)
    - [_Question 29._](maze.md#question-29-toc)
    - [_Question 30._](maze.md#question-30-toc)
    - [_Question 31._](maze.md#question-31-toc)
  - [_solving perfect mazes_](maze.md#solving-perfect-mazes-toc)
    - [_Question 32._](maze.md#question-32-toc)
    - [_Question 33._](maze.md#question-33-toc)
- [_Quad trees_ (_optional_)](qtree.md#quad-trees-toc-optional)
  - [_$n$-body collision simulation_](qtree.md#n-body-collision-simulation-toc)
    - [_Question 34._](qtree.md#question-34-toc)
    - [_Question 35._](qtree.md#question-35-toc)
    - [_Question 36._](qtree.md#question-36-toc)
  - [_Let's speed things up_](qtree.md#lets-speed-things-up-toc)
    - [_Question 37._](qtree.md#question-37-toc)
    - [_Question 38._](qtree.md#question-38-toc)
    - [_Question 39._](qtree.md#question-39-toc)
    - [_Question 40._](qtree.md#question-40-toc)
    - [_Question 41._](qtree.md#question-41-toc)
    - [_Question 42._](qtree.md#question-42-toc)
    - [_Question 43._](qtree.md#question-43-toc)
    - [_Question 44._](qtree.md#question-44-toc)
    - [_Question 45._](qtree.md#question-45-toc)

![trees](assets/trees.png)

---

## Upload your work to the LMS [[toc](#table-of-content)]
- open a terminal
- go into the folder containing your project
- use the `zip` command to compress your project
```shell
zip -r project.zip . -x "venv/**" ".git/**"
```
- upload the ZIP archive to the [LMS](https://lms.isae.fr/mod/assign/view.php?id=116610&action=editsubmission)

---

## Binary search [[toc](#table-of-content)]
Let's start this class with a quite simple problem: given a list $l$ of $n$
integers and an integer $i$, how can you tell whether or not $i$ is in $l$?

:gear: You can run `pytest`. You should have 14 failing tests, that's
normal you will complete your code base and make sure the tests pass during the
class.

### Question 1. [[toc](#table-of-content)]
:file_folder: Create a new file `search.py`.

:pencil: let's create a big list, and suppose it is not sorted. Using the module `time`, measure the time to find if the element `199_999_999` is in the list (you can use the keyword `in`).
```python
l = list(range(200_000_000)) 
```

:question: What is the complexity of your algorithm?

From now on, we will assume that the elements in $l$ are sorted in ascending
order.
This will greatly help us improve our search algorithm.

The idea of _binary search_ is the following: because the elements are sorted in
ascending order, if we look at any element $j$ in the middle of a list, if
$i \lt j$ we know that, if $i$ is in $l$, then it must be on the left of $j$!
Now that one half of $l$ has been completely discarded, we can repeat the search
on one of the halfs of $l$.

The algorithm repeats recursively until either
- $i = j$: $i$ is in $l$
- we find the empty list: $i$ is not in $l$

### Question 2. [[toc](#table-of-content)]
:pencil: Implement binary search and measure the time needed to call it.
You should write a function `binary_search` in `search.py`:
- arguments:
  - `l`: a sorted list of integers
  - `i`: an integer
- return: a boolean which tells whether `i` is inside `l` or not

:question: What is the complexity of this new algorithm?

:gear: Run `pytest tests/test_binary_search.py`. If it's all green, good job :clap: :clap:

That's much better! However, we have made a quite huge assumption about the
input data... it should be sorted!!

In the real world, we won't have perfectly sorted data in general and it is
quite expensive to sort lists (remember, comparison-based sorting algorithms are
$O(n \log n)$ at best) or to insert an element (around $O(n)$).

## Binary search trees [[toc](#table-of-content)]

This is where trees, and especially _**B**inary **S**earch **T**ree**s**_ (BSTs),
come into play!

But before getting to BSTs, which will definitely solve our key problem above,
we need to talk about trees.

### The tree data structure [[toc](#table-of-content)]

Trees are a very common algorithmic data structure. A tree is a recursive
structure that can either
- be _empty_
- hold a _value_ and _references_ to other trees

Some naming conventions:
- the _references_ to other trees are called _subtrees_ or _children_ and if there
  are only two such _subtrees_, they are often called _left_ and _right_
- the entry point of the tree is called the _root_
- a tree that has no _subtrees_ is called a leaf
- if $a$ is a tree that has $b$ as a _child_, then $a$ is called the _parent_ of
  $b$

The properties that a tree must satisfy:
- :exclamation: a tree must NOT contain any cycle, i.e. every _node_ in the tree
  should have a unique _parent_

Below are some examples of trees and non-trees:
- the _empty_ tree (yeah, there is nothing to show with that one)
- a single _leaf_
```mermaid
flowchart TD
    0((0))
```
- a tree where all nodes have at most 2 children
```mermaid
flowchart TD
    0((0)); 1((1)); 2((2)); 3((3)); 4((4))
    0 --> 1
    0 --> 2
    1 --> 3
    1 --> 4
```
- a tree with more than 2 children per node
```mermaid
flowchart TD
    0((0)); 1((1)); 2((2)); 3((3)); 4((4)); 5((5)); 6((6)); 7((7))
    0 --> 1
    0 --> 2
    0 --> 3
    0 --> 4
    3 --> 5
    3 --> 6
    3 --> 7
```
- the following _thing_ is NOT a tree
```mermaid
flowchart TD
    0((0)); 1((1)); 2((2)); 3((3)); 4((4))
    0 --> 1
    0 --> 2
    1 --> 3
    1 --> 4
    2 --> 4
```

#### Question 3. [[toc](#table-of-content)]
:file_folder: Create a new file `tree.py`.

:pencil: In `tree.py`, create a new class called `BinaryTree`. It should have
the following attributes which should all default to `None`:
- `value`: an integer
- `left`: another `BinaryTree`, the left child
- `right`: another `BinaryTree`, the right child

:gear: Run `pytest tests/test_binary_tree.py`. You should have 6 failing tests, that's normal.

:pencil: In order to help you debug the code and _see_ the binary trees, please
copy-paste the following method into your `BinaryTree` class. Now, you will be
able to call `print(binary_tree)` and see it in your terminal:
```python
    def __repr__(self) -> str:
        def aux(t, before: str, is_right: bool, has_right_brother: bool):
            if t is None:
                return ''

            if before is None:
                curr = f"{t.value}\n"
            else:
                marker = '`' if is_right else '|'
                curr = before + f"{marker}---- {t.value}\n"

            next = '' if before is None else (
                before + "|     " if has_right_brother else before + "      "
            )
            right = t.right is not None
            if t.left is not None:
                left = aux(
                    t.left, next, is_right=not right, has_right_brother=right
                )
            else:
                left = ''
            if t.right is not None:
                right = aux(
                    t.right, next, is_right=True, has_right_brother=False
                )
            else:
                right = ''
            return curr + left + right

        return aux(self, None, is_right=False, has_right_brother=False).strip()
```

First, we will implement some general methods on binary trees and then we'll
move on to BSTs.

#### Question 4. [[toc](#table-of-content)]
:pencil: Add a method `is_empty` to your `BinaryTree` class. It should return a
`bool`, `True` if the tree is empty and `False` otherwise.

:gear: Run `pytest tests/test_binary_tree.py`. If the "empty" test is green, good job :clap:
:clap:

#### Question 5. [[toc](#table-of-content)]
:pencil: Add a method `is_leaf` to your `BinaryTree` class. It should return a
`bool`, `True` if the tree is a leaf and `False` otherwise.

> :bulb: **Note**
>
> remember the naming conventions from [_The tree data structure_](#the-tree-data-structure-)

:gear: Run `pytest tests/test_binary_tree.py`. If the "leaf" test is green, good job :clap: :clap:

#### Question 6. [[toc](#table-of-content)]
:pencil: At the end of `tree.py`, in a _main_ block, define a variable that
represents the following tree:
```mermaid
flowchart TD
    0((0)); 1((1)); 2((2)); 3((3)); 4((4))
    0 --> 1
    0 --> 2
    1 --> 3
    1 --> 4
```

:question: Can you print the tree to your terminal using `print`?

:question: By using `BinaryTree.is_empty` and `BinaryTree.is_leaf`, is your tree
variable empty? Is it a leaf?

:question: What about the right child of the left child?

### Tree traversal [[toc](#table-of-content)]
In this section, you will learn about the simplest _real_ operation on trees:
_traversal_.

_Traversing_ a tree means going through every node of the tree and performing
an operation on each one of the nodes.

There are four main _traversal_ techniques on binary trees:
- **D**epth **F**irst **S**earch (DFS): the tree is explored as deep as possible
  first
    - prefix: a node is treated before its children
    - infix: a node is treated in between its children
    - postfix: a node is treated after its children
- **B**readth **F**irst **S**earch (BFS): the tree is explored level of depth
  after level of depth

Below is an animation for each one of these _traversal_ techniques:

![](assets/traverse.mp4)

#### Question 7. [[toc](#table-of-content)]
:pencil: Implement the four _traversal_ techniques in your `BinaryTree` class.
At the end of this question, you should have four new methods:
- `BinaryTree.dfs_prefix`
- `BinaryTree.dfs_infix`
- `BinaryTree.dfs_postfix`
- `BinaryTree.bfs`

These methods take as argument a function that will be applied to each node of the tree.

:pencil: Call the `dfs_prefix` on the previous tree by providing the `print` method as the argument. You should get the following list: `0 1 3 4 2`.


:question: With the tree below, compare the order of _traversal_ for each technique.
```mermaid
flowchart TD
    0((0)); 1((1)); 2((2)); 3((3)); 4((4)); 5((5)); 6((6))
    0 --> 1
    0 --> 2
    1 --> 3
    1 --> 4
    2 --> 5
    2 --> 6
```

:question: If you look back at the `BinaryTree.__repr__` method that was given
to you, what is the _traversal_ technique used to achieve the pretty output?

:gear: Use `pytest tests/test_binary_tree.py` to make sure your traversal implementations are
correct :wink:

---
---
> [go to next](bst.md)
