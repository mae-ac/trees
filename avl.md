# Unbalanced trees [[toc](README.md#table-of-content)] (_optional_)

Let's go back to the Binary Search Trees.

We have a problem.

### Question 22. [[toc](README.md#table-of-content)]
:question: What happens if you insert the following values, in that order, in an
empty tree?
```python
values = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
```

:question: What does the tree look like?

:question: What is the consequence on the complexity of all our BST operations?

Fortunately, there is a way to solve this last issue and make our BST quite
good!

# Height-balanced BSTs [[toc](README.md#table-of-content)] (_optional_)

First, let's formalize a bit the intuition from previous section about "_what it
means for a tree to be unbalanced?_"

The height of a tree is the _length of the longest path from the root to the
leaves_.

- the empty tree has a height of $0$
- a leaf has a height of $1$
- the following tree has a height of $3$ because the longest path from the root
    to the leaves is either $0 \rightarrow 3$ or $0 \rightarrow 4$ which are both of
    length $3$
```mermaid
flowchart TD
    0((0)); 1((1)); 2((2)); 3((3)); 4((4))
    0 --> 1
    0 --> 2
    1 --> 3
    1 --> 4
```

:gear: You can run `make test-avl` and you should have 4 failing tests.

## Question 23. [[toc](README.md#table-of-content)]
:pencil: Add a method `height` to the `BinaryTree` class in `tree.py`.

:question: What is the complexity of this algorithm in terms of the number of
nodes in the tree?

:gear: You can run `make test-avl`. If the "height" test is green, good job
:clap: :clap:

Next we can define the _balance_ of a binary tree by the difference of the
height of the right child and the height of the left child.

Below are some examples. All the labels of the nodes in the trees show the
height of the tree on the left and the balance on the right, not the real value
held by the tree, which is not relevant when looking at the _balance_ of a tree.

```mermaid
flowchart TD
    a0(("1, 0"));
    a0

    b5(("3, -1")); b0(("1, 0")); b3(("1, 0")); b7(("1, 0")); b1(("2, 0"));
    b5 --- b1
    b1 --- b3
    b1 --- b0
    b5 --- b7

    c5(("4, -2")); c0(("1, 0")); c3(("2, 1")); c7(("1, 0")); c1(("3, -1"));
    c4(("1, 0"));
    c5 --- c1
    c1 --- c3
    c1 --- c0
    c5 --- c7
    c3 --- c4

    d0(("1, 0")); d1(("2, 1")); d3(("3, 0")); d4(("1, 0")); d5(("2, 0"));
    d7(("1, 0"));
    d3 --- d5
    d3 --- d1
    d1 --- d0
    d5 --- d4
    d5 --- d7
```
```mermaid
flowchart TD
    e1(("4, -1")); e2(("3, 0")); e3(("2, 1")); e4(("2, 0")); e5(("2, 0"));
    e6(("1, 0")); e7(("1, 0")); e8(("1, 0")); e9(("1, 0")); e10(("1, 0"));
    e1 --- e2
    e1 --- e3
    e3 --- e6
    e2 --- e4
    e2 --- e5
    e4 --- e7
    e4 --- e8
    e5 --- e9
    e5 --- e10
```

## Question 24. [[toc](README.md#table-of-content)]
:pencil: Write a method `balance` to `BinaryTree` to compute the _balance_ of
a tree.

:gear: You can run `make test-avl`. If the "balance" test is green, good job
:clap: :clap:

:question: Looking at the example trees above, what could be a _good_ definition
of a _balanced_ tree?

> :bulb: **Hint**
>
> the trees are symmetric from left to right, so the _balance_ being positive
> or negative does not matter

## Question 25. [[toc](README.md#table-of-content)]
:pencil: Write a method `is_balanced` to `BinaryTree` to tell whether a tree is
balanced or not.

:gear: You can run `make test-avl`. If the "is_balanced" test is green, good job
:clap: :clap:

## Tree rotations [[toc](README.md#table-of-content)]

If a tree is _unbalanced_, it is possible to fix the balance by applying a
_rotation_ on the root of the tree.

There are two types of rotations, left and right, depending on the side where
the balance is wrong.

As illustrated in the animation below, a right rotation consists of
- bubbling up the left child as the new root
- moving the previous root to the right child of the new root
- correcting the BST property

![](assets/rotate.mp4)

### Question 26. [[toc](README.md#table-of-content)]
:pencil: Implement the two tree rotations.

:gear: You can run `make test-avl`. If the "rotate right" and "rotate left"
tests are green, good job :clap: :clap:

In order to help keeping track of the height of each node in the tree without
recomputing it all the time, we will add the height to our BST class and
recompute it after each operation on the tree.

:pencil: Add a field `height` to the BST class.

:pencil: Recompute the `height` after `insert`, `delete` and the rotations.

Below is the pseudo-algorithm of the tree "rebalancing" operation:
```js
1 rebalance(t)
2     if t.balance() < -1 and t.left.balance() == -1 then t.rotate_right()
3     else if t.balance() > 1 and t.right.balance() == 1 then t.rotate_left()
4     else if t.balance() < -1 and t.left.balance() == 1 then t.rotate_left_right()
5     else if t.balance() > 1 and t.right.balance() == -1 then t.rotate_right_left()
```

Where the "right-left" rotation consists of
- rotating the right subtree to the right
- rotating the root to the left

and the "left-right" rotation consists of
- rotating the left subtree to the left
- rotating the root to the right

### Question 27. [[toc](README.md#table-of-content)]
:pencil: Implement "left-right" and "right-left" rotations.

:pencil: Implement the `rebalance` method for the BST class.

## Balancing our BSTs [[toc](README.md#table-of-content)]

### Question 28. [[toc](README.md#table-of-content)]
:pencil: Call `rebalance` at the end of `insert` and `delete`.

:question: Can you run the same example as in the beginning, i.e. inserting
integers in ascending order and make sure that the tree is balanced at the end?

---
---
> [go to next](maze.md)
