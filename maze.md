## Maze generation and solving [[toc](README.md#table-of-content)] (_optional_)

In this section, you will have to produce some code that will (1) generate a
_perfect_ maze and (2) solve the maze.

But, first of all, what is a _maze_? And what does it mean for a maze to be
_perfect_?

Typically a _maze_ is defined on a grid of cells and generating a maze is the
same as deciding whether two adjacent cells should be connected or separated by
a wall.

A _perfect_ maze is simply a maze that have (1) no cycle and (2) no isolated
parts.

These two statements are equivalent to the following property: _any two cells in
a perfect maze should be connected by exactly one path_.

Below is an example of a _perfect_ maze:

![maze](assets/maze.jpg)

Thanks to the _perfect maze_ property, if we choose one of the cells to be the
_root_, then the _maze_ is simply a tree!

![maze-tree](assets/maze-tree.jpg)

> :bulb: **Note**
>
> In the image above, the green cell can be chosen to be the root. It is then
> possible to _unfold_ the maze into a tree where the root is at the top.
>
> Thanks to the _perfect maze_ property, it is possible to find the unique path
> between the two purple cells.

We can thus apply our new knowledge about trees to _perfect mazes_! :tada:

In order to visualize the maze generation and the maze solving, you can use the
[`src.ui.maze` library](src/README.md#srcuimaze) provided with the class material.

:mag: Read the document to familiarize yourself with how to use the library in
code and what the keybindings and actions are in the visualization.

### Generating _perfect_ mazes [[toc](README.md#table-of-content)]

As said earlier, our mazes will live in a rectangular grid of square cells.

Let's assume the size of the grid is $n$ rows by $m$ columns.

The cells will be identified by a number, starting from $0$ up to $nm - 1$, $0$
being the top-left cell and $nm - 1$ being the bottom-right one.

The first thing we need to do is compute the neighbours of any given cell $c$.

> :exclamation: **Important**
>
> most of the cells will have $4$ neighbours, but be careful with the borders
> and corners of the grid!

#### Question 29. [[toc](README.md#table-of-content)]
:file_folder: Create a new file `maze.py`.

:pencil: As shown in the documentation of `src.ui.maze`, create a small maze, a
canva and run `canva.loop` in an infinite loop.

> :bulb: **Note**
>
> for now, you can omit the return value of `canva.loop` and not bother about
> the example `path` function.

#### Question 30. [[toc](README.md#table-of-content)]
:pencil: Write a function `neighbours` that will compute the list of all valid
neighbours for a given cell. It should have the following signature:
- arguments:
  - `c`: the index of the cell
  - `w`: the width of the grid
  - `h`: the height of the grid
- return: the list of indices of all valid neighbours of `c`

#### Question 31. [[toc](README.md#table-of-content)]
:pencil: Write an iterative `build` function using a _depth-first_ approach to
build the list of edges in the maze.

It should take as arguments an empty maze, a starting cell and a _callback_
function that takes a dictionary as input and don't return anything.

Call your new `build` function on your `maze`, with a random starting point and
with the following callback:
```python
lambda m: canva.step(m)
```
where `m` is the maze at any time of the generation.

If you see a maze building itself, congratulations, your `build` is working
:clap: :clap:

### Solving _perfect_ mazes [[toc](README.md#table-of-content)]
Now that we have a complete valid _perfect_ maze, which is also a tree, we can
use our knowledge about trees to solve it.

More specifically, we can use tree traversal algorithms such as DFS and BFS to
find a path between two cells in the maze.

#### Question 32. [[toc](README.md#table-of-content)]
:pencil: Write a `path` function that will compute the path between two cells
in a maze using a _depth-first_ approach.

Using the [documentation of `src.ui.maze`](src/README.md#srcuimaze) and the way `canva` will return signals
containing the start and end cells selected with the mouse, call your new `path`
function on your `maze`, using the start and end cells returned by the canva and
the following callback:
```python
lambda m, p, v, n: canva.step(m, p, v, n, complete=False)
```
where `m` is the maze, `p` is an optional path of cells, `v` is an optional list
of already visited cells and `n` is an optional list of next cells to visit.

#### Question 33. [[toc](README.md#table-of-content)]
:pencil: Can you adapt the `path` function to use a _breadth-first_ approach?

---
---
> [go to next](qtree.md)
