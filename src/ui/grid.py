import pygame
import logging
from collections import namedtuple
from typing import Tuple

_VALID_SQUARE_GRID_STYLES = ["lines", "cells"]

SquareGridStyle = namedtuple(
    "SquareGridStyle",
    ["type", 'm', 'r', 'w'],
    defaults=["lines", .9, .1, 5],
)


def square(
    screen: pygame.surface.Surface,
    width: int,
    height: int,
    size: int,
    color: Tuple[int, int, int],
    style: SquareGridStyle = SquareGridStyle(),
):
    if style.type not in _VALID_SQUARE_GRID_STYLES:
        logging.error(
            f"invalid square grid style '{style}', expected one of {_VALID_SQUARE_GRID_STYLES}"
        )
        return

    if style.type == "lines":
        w, h = screen.get_size()
        for i in range(1, height):
            pygame.draw.line(
                screen, color, (0, i * size), (w, i * size), width=style.w
            )
        for j in range(1, width):
            pygame.draw.line(
                screen, color, (j * size, 0), (j * size, h), width=style.w
            )
    elif style.type == "cells":
        d = (style.m * size) // 2
        s = int(size * style.m)
        for i in range(height):
            for j in range(width):
                cell(screen, i, j, size, color, d, style.r, s)


def cell(
    screen: pygame.surface.Surface,
    i: int,
    j: int,
    size: int,
    color: Tuple[int, int, int],
    d: int,
    r: int,
    s: int,
):
    c_x, c_y = int((j + .5) * size), int((i + .5) * size)

    pygame.draw.rect(
        screen,
        color,
        (c_x - d, c_y - d, s, s),
        border_radius=int(size * r),
    )
