from dataclasses import dataclass

import pygame
import sys

COLORS = {
    "background": (0, 0, 0),
    "qtree box": (255, 255, 255),
    "colliding ball": (255, 255, 255),
    "ball": (100, 100, 100),
    "qtree query range": (0, 255, 0),
}


@dataclass
class Canva:
    width: int
    height: int
    caption: str
    frame_rate: int
    screen: pygame.surface.Surface = None
    clock: pygame.time.Clock = None

    def setup(self):
        pygame.init()

        self.screen = pygame.display.set_mode((self.width, self.height))
        pygame.display.set_caption(self.caption)

        self.clock = pygame.time.Clock()

        self.mouse = None
        self.points = []

    def __handle_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT or (
                event.type == pygame.KEYDOWN and
                event.key == pygame.K_ESCAPE
            ):
                pygame.quit()
                sys.exit()
            elif event.type == pygame.MOUSEMOTION:
                self.mouse = event.pos

    def __render_qtree(self, qtree):
        x = qtree.boundary.x
        y = qtree.boundary.y
        w = qtree.boundary.w
        h = qtree.boundary.h
        pygame.draw.rect(
            self.screen,
            COLORS["qtree box"],
            (x - w / 2, y - h / 2, w, h),
            width=1,
        )

        if qtree.divided:
            self.__render_qtree(qtree.ne)
            self.__render_qtree(qtree.nw)
            self.__render_qtree(qtree.se)
            self.__render_qtree(qtree.sw)

    def __render(self, f):
        self.screen.fill(COLORS["background"])

        res = f(self.points, self.mouse)
        if isinstance(res, list) and isinstance(res[0], int):
            for p, c in zip(self.points, res):
                if c == 0:
                    color = (0, 255, 0)
                elif c == 1:
                    color = (128, 255, 0)
                elif c == 2:
                    color = (255, 255, 0)
                elif c == 3:
                    color = (255, 128, 0)
                else:
                    color = (255, 0, 0)
                pygame.draw.circle(self.screen, color, (p.x, p.y), p.r)
        else:
            qtree, range = res
            self.__render_qtree(qtree)

            for p in self.points:
                pygame.draw.circle(
                    self.screen, COLORS["colliding ball"], (p.x, p.y), p.r
                )

            if range is not None:
                pygame.draw.rect(
                    self.screen,
                    COLORS["qtree query range"],
                    (range.x - range.w / 2, range.y - range.h / 2, range.w, range.h),
                    width=1,
                )

            for p in qtree.query(range):
                pygame.draw.circle(
                    self.screen,
                    COLORS["qtree query range"],
                    (p.x, p.y),
                    p.r,
                )

        pygame.display.flip()

    def step(self, u, f):
        self.__handle_events()
        self.points = u(self.points)
        self.__render(f)
        self.clock.tick(self.frame_rate)

    def loop(self, u, f):
        while True:
            self.step(u, f)
