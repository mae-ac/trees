def test_height():
    from tree import BinaryTree

    assert BinaryTree().height() == 0
    assert BinaryTree(0).height() == 1
    assert BinaryTree(
        0,
        BinaryTree(
            1,
            BinaryTree(3),
            BinaryTree(4),
        ),
        BinaryTree(2),
    ).height() == 3


def test_balance():
    from tree import BinaryTree

    assert BinaryTree().balance() == 0
    assert BinaryTree(0).balance() == 0
    assert BinaryTree(
        0,
        BinaryTree(
            1,
            BinaryTree(3),
            BinaryTree(4),
        ),
        BinaryTree(2),
    ).balance() == -1


def test_rotate_right():
    from tree import BinaryTree

    assert BinaryTree().rotate_right() == BinaryTree()
    assert BinaryTree(0).rotate_right() == BinaryTree(0)
    t = {
        "before": BinaryTree(
            10,
            BinaryTree(
                5,
                BinaryTree(1),
                BinaryTree(8),
            ),
            BinaryTree(20),
        ),
        "after": BinaryTree(
            5,
            BinaryTree(1),
            BinaryTree(
                10,
                BinaryTree(8),
                BinaryTree(20),
            ),
        )
    }
    assert t["before"].rotate_right() == t["after"]


def test_rotate_left():
    from tree import BinaryTree

    assert BinaryTree().rotate_left() == BinaryTree()
    assert BinaryTree(0).rotate_left() == BinaryTree(0)
    t = {
        "before": BinaryTree(
            10,
            BinaryTree(5),
            BinaryTree(
                20,
                BinaryTree(15),
                BinaryTree(25),
            ),
        ),
        "after": BinaryTree(
            20,
            BinaryTree(
                10,
                BinaryTree(5),
                BinaryTree(15),
            ),
            BinaryTree(25),
        )
    }
    assert t["before"].rotate_left() == t["after"]
