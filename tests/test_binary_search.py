from random import randint


def test_binary_search():
    from search import binary_search

    values = sorted([randint(0, 1000) for _ in range(100)])

    for v in values:
        assert binary_search(values, v), f"{v} should be in {values}"

    for v in [randint(1000, 2000) for _ in range(100)]:
        assert not binary_search(values, v), f"{v} should NOT be in {values}"
